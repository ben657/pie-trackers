import React, {useState, useEffect} from 'react'; 
import ReactDOM from 'react-dom';

const Pie = ({pie}) => {
	console.log(pie);
	return (
		<div style={{
			width: '350px',
			height: '350px',
			margin: '32px',
			position: 'relative'
		}}>
			<img src={pie.featured_image.grande} style={{
				width: '100%',
				height: '100%'
			}}></img>
			<div style={{
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				backgroundColor: pie.is_sold_out ? 'rgba(128, 0, 0, 0.5)' : 'rgba(0, 128, 0, 0.5)',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				fontSize: '2.3rem',
				color: 'white',
				fontFamily: 'Arial'
			}}>
				{pie.is_sold_out ? 'Out of stock' : 'In stock!'}
			</div>
		</div>
	);
}

const App = () => {
	const [pies, setPies] = useState([]);

	async function getPies() {
		const result = await fetch(`https://sdk.selz.com/products/all/229456?q=&c=5e7f1997701f5d0d84909aa7&p=1`);
		const {data: {products}} = await result.json();
		const anyStock = products.some(p => !p.is_sold_out);
		setPies(products);
		document.title = `Pie jackers ${anyStock ? 'has' : 'doesn\'t have'} stock`;
		setTimeout(() => {
			getPies();
		}, 30000);
	}

	useEffect(() => {
		getPies();
	}, []);

	return (
		<div style={{
			display: 'flex',
			flexFlow: 'row wrap',
			width: '100vw',
			minHeight: '100vh',
			alignItems: 'center',
			justifyContent: 'center'

		}}>
			{pies.map(p => <Pie key={p.id} pie={p}/>)}
		</div>
	);
}

ReactDOM.render(<App/>, document.getElementById('root'));